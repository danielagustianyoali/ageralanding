<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingController extends Controller
{
    //
    public function homepage(){
        return view('pages.homepage');
    }
    public function successPage(){
        $link = "https://meet.google.com";
        return view('pages.success-new', compact('link'));
    }
    public function homepage2(){
        return view('pages.homepage-new');
    }
}

<!--begin::Entry-->
@extends('master')

@section('cssinline')

@endsection


@section('content')
    <div class="d-flex flex-column flex-column-fluid">

        <!--begin::Container-->


        <div>
            <section class="banner-hero d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 my-auto">
                            <h1 class="title">Peluang Keuntungan Bisnis Tahun
                                2022 Kini Sudah Di Depan Mata, Bisnis
                                Valuta Asing Kini Semakin Mudah
                                Dengan <span>Signal Trading Agera</span> </h1>
                            <div class="button-bar mt-7 mb-7">
                                <button class="btn button_prim_circled">
                                    Sign Up
                                </button>
                            </div>
                        </div>
                        <div class="col-md-6 my-auto mx-auto">
                            <img src="{{ asset('asset/images/laptop.png') }}" alt="" class="banner-img my-10">
                        </div>
                    </div>
                </div>
            </section>
            <div class="divider"></div>
            <section class="section-2">
                <div class="container">
                    <h2 class="text-center title-part mb-6"> Apakah Anda Sering Mengalami Ini?</h2>
                    <div class="card">
                        <div class="card-body">
                            <ul>
                                <li>Sudah ikut banyak signal trading berbayar tapi belum untung juga?
                                </li>
                                <li>Pakai robot trading, harga puluhan juta tapi hasil sangat minim bahkan Margin
                                    Call?
                                </li>
                                <li>Merasa bingung sendiri ingin untung tapi takut dengar banyak orang rugi dari
                                    bisnis ini?</li>
                                <li>Trading bertahun-tahun tapi belum profit konsisten, atau malah sering rugi?
                                </li>
                                <li>Punya banyak ilmu trading tapi belum pernah dapat keuntungan maksimal?</li>
                                <li>Sudah jago analisa tetapi belum kaya juga?</li>
                                <li>Sering trading terbawa emosi, ingin untung malah buntung?</li>
                                <li> Habiskan banyak uang puluhan juta untuk ikut seminar atau workshop yang
                                    menjanjikan angin surga tetapi belum ada hasil?</li>


                            </ul>
                            <h4 class="title-sub mt-8">TENANG, Jangan Khawatir Anda Tidak Sendirian.
                            </h4>
                            <h4 class="title-sub">Ketika Anda mulai menggunakan <span>SIGNAL TRADING GAJAHMADA</span> ,
                                pelan tapi pasti
                                kehidupan Anda akan mulai berubah menjadi BERUNTUNG.
                            </h4>
                        </div>
                    </div>
                </div>
            </section>
            <div class="divider"></div>
            <section class="section-3">
                <div class="inner-padding"></div>
                <div class="container">
                    <div class="row ">
                        <div class="col-md-6 my-auto">
                            <h3 class="title-part text-center mb-6 ">
                                Manfaat Berlangganan <span>Signal Trading Agera</span>
                            </h3>
                            <div class="d-flex justify-content-center">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row num-row">
                                            <div class="col-lg-2 col-3 col-3 ">
                                                <div class="circle-number">
                                                    <h4 class="num">1</h4>
                                                </div>

                                            </div>
                                            <div class="col-lg-10 col-9 col-9 ">
                                                <h4 class="title">Mudah</h4>
                                                <h6 class="detail">Sinyal mudah dipelajari
                                                    dan akan dipandu oleh
                                                    Expert Trader Kami</h6>
                                            </div>
                                        </div>

                                        <div class="row num-row">
                                            <div class="col-lg-2 col-3 ">
                                                <div class="circle-number">
                                                    <h4 class="num">2</h3>
                                                </div>

                                            </div>
                                            <div class="col-lg-10 col-9 ">
                                                <h4 class="title">Konsisten
                                                </h4>
                                                <h6 class="detail">Selalu konsisten
                                                    profitnya sesuai
                                                    dengan trend market</h6>
                                            </div>
                                        </div>
                                        <div class="row num-row">
                                            <div class="col-lg-2 col-3 ">
                                                <div class="circle-number">
                                                    <h4 class="num">3</h4>
                                                </div>

                                            </div>
                                            <div class="col-lg-10 col-9 ">
                                                <h4 class="title">Akurat</h4>
                                                <h6 class="detail">Sinyal trading agera
                                                    selalu akurat dan
                                                    terbukti menghasilkan profit</h6>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-md-6 my-auto">
                            <img src="{{ asset('asset/images/nonton-utub.png') }}
                                " alt="image" class="img-section-3">
                        </div>
                    </div>
                </div>
                <div class="inner-padding"></div>
            </section>
            <div class="divider"></div>
            <section class="section-4">
                <div class="container position-relative">
                    <h3 class="title-part title-mobile text-center">Berapa harga untuk
                        berlangganan Signal
                        Trading Agera?</h3>
                    <div class="row">
                        <div class="col-md-6 my-auto mx-auto mb-5">
                            <img src="{{asset('asset/images/phone_mock.png')}}"
                                alt="image" class="img-section-4">
                        </div>
                        <div class="col-md-6 my-auto mb-4">
                            <div>

                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <h4 class="title-part text-center">
                                        Hanya dengan 1.5 juta/bulan, Anda sudah mendapatkan <span>SIGNAL
                                            TRADING AGERA</span>
                                    </h4>
                                    <h3 class="subtitle text-center">BAKAL CUAN TERUS!</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="position-absolute bubble-div">
                        <div class="bubble bubble-bottom-left">
                            Berapa harga untuk
                            berlangganan Signal
                            Trading Agera?
                        </div>
                    </div>
                </div>

            </section>
            <div class="divider"></div>
            <section class="section-form">
                <div class="inner-padding"></div>
                <div class="container">
                    <div class="row ">
                        <div class="col-md-6 order-md-1 order-2">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="title-form mb-6">Tunggu Apalagi? Segera Book Jadwal Anda Untuk
                                        Bertemu Expert Trader Kami & Buktikan
                                        Keuntungannya Bersama Kami!
                                    </h4>

                                    <form action="" class="register-form">

                                        <p class="title-input">Nama<span>*</span>
                                        </p>
                                        <input type="text" class="form-control" name="name" placeholder="Nama">
                                        <p class="title-input">Email<span>*</span>
                                        </p>
                                        <input type="text" class="form-control" name="email" placeholder="Email">
                                        <p class="title-input">Nomor Telepon<span>*</span>
                                        </p>
                                        <input type="text" class="form-control" name="telepon"
                                            placeholder="Nomor Telepon">
                                        <p class="title-input">Tanggal Bertemu<span>*</span>
                                        </p>
                                        <input type="date" class="form-control" name="date"
                                            placeholder="Pilih Tanggal (mm/dd/yy)">
                                        <p class="title-input">Jam Bertemu<span>*</span>
                                        </p>
                                        <input type="time" class="form-control" name="name"
                                            placeholder="Pilih Jam (mm/dd/yy)">
                                        <div class="d-flex justify-content-center mt-7 ">
                                            <button class="btn btn__prim">Daftar Kelas Mentoring</button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 order-md-2 order-1">
                            <img src="{{asset('asset/images/calendar.png')}}"
                                alt="" class="img-section-form">
                        </div>
                    </div>
                </div>
                <div class="inner-padding"></div>
            </section>

            <!--[html-partial:begin:{"id":"demo2/dist/inc/view/demos/pages/index","page":"index"}]/-->

            <!--[html-partial:begin:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo2","page":"index"}]/-->

            <!--begin::Dashboard-->

            <!--begin::Row-->


            <!--end::Row-->

            <!--end::Dashboard-->

            <!--[html-partial:end:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo2","page":"index"}]/-->

            <!--[html-partial:end:{"id":"demo2/dist/inc/view/demos/pages/index","page":"index"}]/-->
        </div>

        <!--end::Container-->
    </div>
@endsection


@section('jsPage')

@endsection


<!--end::Entry-->

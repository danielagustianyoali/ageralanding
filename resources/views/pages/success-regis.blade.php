<!--begin::Entry-->
@extends('master')

@section('cssinline')

@endsection


@section('content')
    <div class="d-flex flex-column-fluid">

        <!--begin::Container-->
        <div class="container">

            <!--[html-partial:begin:{"id":"demo2/dist/inc/view/demos/pages/index","page":"index"}]/-->

            <!--[html-partial:begin:{"id":"demo1/dist/inc/view/partials/content/dashboards/demo2","page":"index"}]/-->

            <!--begin::Dashboard-->

            <!--begin::Row-->
            <div class="container">
                <div class="card mt-7 mb-7">
                    <div class="card-body">
                        <div class="success-div ">
                            <div class="row">
                                <div class="col-lg-7 col-md-6 my-auto">
                                    <div class="card">
                                        <div class="card-body">
                                            <h4 class="details mb-5">
                                                Anda mendapatkan link meeting dengan expert
                                                trader kami (Coach Wahyu Laksono) di bawah
                                                ini, sesuai dengan tanggal dan waktu yang telah
                                                anda pilih sebelumnya.
                                            </h4>
                                            <h4>
                                                <a href="{{ $link }}" class="text-center link-zoom">Link Meeting
                                                    Zoom</a>
    
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-6 my-auto">
                                    <img src="{{asset('asset/images/grab-handphone.png')}}"
                                        alt="" class="img-success">
                                </div>
                            </div>
    
                        </div>
                    </div>
                </div>
    
            </div>
            

         
        </div>

        
    </div>
@endsection


@section('jsPage')

@endsection


<!--end::Entry-->

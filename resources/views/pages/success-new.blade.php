@extends('master2')


@section('content')
    <section id="content">

        <div class="content-wrap">
            <div class="container">
                <div class="container">
                    <div class="card mt-7 mb-7" style="border: 2px solid #126f64">
                        <div class="card-body">
                            <div class="success-div ">
                                <div class="row">
                                    <div class="col-lg-7 col-md-6 my-auto">
                                        {{-- <div class="card" style="">
                                        <div class="card-body"> --}}
                                        <h4 class="details mb-5">
                                            Anda mendapatkan link meeting dengan expert
                                            trader kami (Coach Wahyu Laksono) di bawah
                                            ini, sesuai dengan tanggal dan waktu yang telah
                                            anda pilih sebelumnya.
                                        </h4>
                                        <h4>
                                            <a href="{{ $link }}" class="text-center link-zoom">Link Meeting
                                                Zoom</a>    x   

                                        </h4>
                                        {{-- </div>
                                    </div> --}}
                                    </div>
                                    <div class="col-lg-5 col-md-6 my-auto">
                                        <img src="{{ asset('asset/images/grab-handphone.png') }}" alt=""
                                            class="img-success">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

@endsection

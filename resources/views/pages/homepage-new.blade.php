@extends('master2')

@section('banner')
    <div id="slider" class="slider-element dark py-5">

        <div class="container">
            <div class="row">
                <div class="col-md-6 ">
                    <h2 class=" color fw-normal ">Peluang Keuntungan Bisnis Tahun
                        2022 Kini Sudah Di Depan Mata, Bisnis
                        Valuta Asing Kini Semakin Mudah
                        Dengan <strong>Signal Trading Agera</strong> </h2>
                    <p class="color">Objectively harness robust ROI via functional leadership skills. Holisticly
                        create one-to-one models.</p>
                    <button href="#"
                        class="btn text-larger rounded-pill bg-color text-white px-4 py-2 h-op-09 op-ts btn-sign-up">Sign
                        Up</button>
                    {{-- <a href="#" class="btn text-larger rounded-pill px-4 py-2 ms-2 border-color color h-op-09 op-ts">Download
                        App</a> --}}
                </div>
                <div class="col-md-6">
                    <img src="{{ asset('asset/images/laptop.png') }}" alt="" class="banner-img my-10">
                </div>
            </div>
        </div>
    </div>
@endsection


@section('content')
    <section id="content">
        <div class="content-wrap">

            <div class="container">

                <!-- How We Care
                                                                                ============================================= -->
                <div class="row justify-content-center mt-5">
                    <div class="col-md-7 text-center">
                        <h3 class="display-6 color fw-bold " style="margin-bottom: 15px">Apakah Anda Sering
                            Mengalami Ini?</h3>
                    </div>
                </div>

                <div class="row justify-content-center align-items-center gutter-50 col-mb-80 mt-2">
                    <div class="col-xl-12 col-lg-12">
                        <div class="row feature-box-border col-mb-30 justify-content-center align-items-center">
                            @component('components.custom-card')
                                @slot('id')
                                    1
                                @endslot
                                @slot('title')
                                    Sudah ikut banyak signal trading berbayar tapi belum untung juga?
                                @endslot
                            @endcomponent
                            @component('components.custom-card')
                                @slot('id')
                                    2
                                @endslot
                                @slot('title')
                                    Pakai robot trading, harga puluhan juta tapi hasil
                                    sangat minim bahkan Margin
                                Call? @endslot
                            @endcomponent
                            @component('components.custom-card')
                                @slot('id')
                                    3
                                @endslot
                                @slot('title')
                                    Merasa bingung sendiri ingin untung tapi takut dengar
                                    banyak orang rugi dari
                                bisnis ini?@endslot
                            @endcomponent
                            @component('components.custom-card')
                                @slot('id')
                                    4
                                @endslot
                                @slot('title')
                                    Trading bertahun-tahun tapi belum profit konsisten,
                                atau malah sering rugi?@endslot
                            @endcomponent
                            @component('components.custom-card')
                                @slot('id')
                                    5
                                @endslot
                                @slot('title')
                                    Punya banyak ilmu trading tapi belum pernah dapat
                                keuntungan maksimal?@endslot
                            @endcomponent
                            @component('components.custom-card')
                                @slot('id')
                                    6
                                @endslot
                                @slot('title')
                                Sudah jago analisa tetapi belum kaya juga?@endslot
                            @endcomponent
                            @component('components.custom-card')
                                @slot('id')
                                    7
                                @endslot
                                @slot('title')
                                    Sering trading terbawa emosi, ingin untung malah
                                buntung?@endslot
                            @endcomponent
                            @component('components.custom-card')
                                @slot('id')
                                    8
                                @endslot
                                @slot('title')
                                    Habiskan banyak uang puluhan juta untuk ikut seminar
                                    atau workshop yang
                                menjanjikan angin surga tetapi belum ada hasil?@endslot
                            @endcomponent



                        </div>
                    </div>
                </div>

                <!-- Get 24/7 Care
                                                                                                                                ============================================= -->
                <div class="row justify-content-between align-items-center my-6 col-mb-50 ">
                    <div class="col-md-6 mr-2 order-md-1 order-2">
                        <h3 class="display-5 color fw-normal  mb-5"> Manfaat Berlangganan <strong>Signal Trading
                                Agera</strong>
                        </h3>

                        <div class="feature-box fbox-sm mb-4">
                            <div class="fbox-icon">
                                <i class="bg-color-50 color icon-line-calendar"></i>
                            </div>
                            <div class="fbox-content">
                                <h3 class="nott text-larger fw-normal mb-2 color"> <strong>Mudah</strong> </h3>
                                <p>Sinyal mudah dipelajari
                                    dan akan dipandu oleh
                                    Expert Trader Kami</p>
                            </div>
                        </div>
                        <div class="feature-box fbox-sm mb-4">
                            <div class="fbox-icon">
                                <i class="bg-color-50 color icon-line-calendar"></i>
                            </div>
                            <div class="fbox-content">
                                <h3 class="nott text-larger fw-normal mb-2 color"> <strong>Konsisten</strong> </h3>
                                <p>Selalu konsisten
                                    profitnya sesuai
                                    dengan trend market</p>
                            </div>
                        </div>
                        <div class="feature-box fbox-sm mb-4">
                            <div class="fbox-icon">
                                <i class="bg-color-50 color icon-line-calendar"></i>
                            </div>
                            <div class="fbox-content">
                                <h3 class="nott text-larger fw-normal mb-2 color"> <strong>Akurat</strong> </h3>
                                <p> <strong class="color">Sinyal Trading Agera</strong>
                                    selalu akurat dan
                                    terbukti menghasilkan profit</p>
                            </div>
                        </div>






                        <a href="#"
                            class="color fw-bold border-bottom border-width-2 border-color me-2 text-capitalize">Learn
                            more
                            about our Support</a><i class="icon-chevron-right icon-stacked text-smaller color"></i>
                    </div>
                    <div class="col-lg-6 my-auto order-md-2 order-1">
                        <img src="{{ asset('asset/images/nonton-utub.png') }}
                                    " alt="image" class="img-section-3 rounded">
                    </div>
                </div>

                <!-- Section Mobile
                                                                                                                                ============================================= -->


                <!-- Prevention of Covid-19
                                                                                                                                ============================================= -->


            </div>

            <!-- Download App Section
                                                                                                                            ============================================= -->
            <section class="section-4">
                <div class="container position-relative">
                    <h3 class="display-5 color fw-normal  title-mobile text-center">Berapa harga untuk
                        berlangganan <strong>Signal
                            Trading Agera</strong> ?</h3>
                    <div class="row">
                        <div class="col-md-6 my-auto mx-auto mb-5">
                            <img src="{{ asset('asset/images/phone_mock.png') }}" alt="image" class="img-section-4">
                        </div>
                        <div class="col-md-6 my-auto ">
                            <div>

                            </div>

                            <div class="card">
                                <div class="card-body">
                                    <h4 class="display-7 title-part text-center">
                                        Hanya dengan 1.5 juta/bulan, Anda sudah mendapatkan <span>SIGNAL
                                            TRADING AGERA</span>
                                    </h4>
                                    <h3 class="subtitle text-center display-6">BAKAL CUAN TERUS!</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="position-absolute bubble-div">
                        <div class="bubble bubble-bottom-left">
                            Berapa harga untuk
                            berlangganan Signal
                            Trading Agera?
                        </div>
                    </div>
                </div>

            </section>
           
            <section class="section-form">
                <div class="inner-padding"></div>
                <div class="container">
                    <div class="row ">
                        <div class="col-md-6 order-md-1 order-2">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="title-form display-7">Tunggu Apalagi? Segera Book Jadwal Anda Untuk
                                        Bertemu Expert Trader Kami & Buktikan
                                        Keuntungannya Bersama Kami!
                                    </h4>

                                    <form action="" class="register-form">

                                        <p class="title-input">Nama<span>*</span>
                                        </p>
                                        <input type="text" class="form-control" name="name" placeholder="Nama">
                                        <p class="title-input">Email<span>*</span>
                                        </p>
                                        <input type="text" class="form-control" name="email" placeholder="Email">
                                        <p class="title-input">Nomor Telepon<span>*</span>
                                        </p>
                                        <input type="text" class="form-control" name="telepon"
                                            placeholder="Nomor Telepon">
                                        <p class="title-input">Tanggal Bertemu<span>*</span>
                                        </p>
                                        <input type="date" class="form-control" name="date"
                                            placeholder="Pilih Tanggal (mm/dd/yy)">
                                        <p class="title-input">Jam Bertemu<span>*</span>
                                        </p>
                                        <input type="time" class="form-control" name="name"
                                            placeholder="Pilih Jam (mm/dd/yy)">
                                        <div class="d-flex justify-content-center mt-7 ">
                                            <button class="btn btn__prim">Daftar Kelas Mentoring</button>

                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 order-md-2 order-1">
                            <img src="{{asset('asset/images/calendar.png')}}"
                                alt="" class="img-section-form rounded">
                        </div>
                    </div>
                </div>
                <div class="inner-padding"></div>
            </section> 
        </div>
    </section>
@endsection
@section('js')
    
@endsection
<div class="col-lg-9 col-md-10 feature-box fbox-border fbox-light fbox-effect">
    <div class="d-flex">
        <div class="fbox-icon">
            <a href="#"><i>{{$id}}</i></a>
        </div>
        <div class="fbox-content d-flex align-items-center" style="flex-basis:none !important">
            <h4 class="nott text-larger mb-2">{{$title}}</h4>
    
        </div>
    </div>
    
</div>